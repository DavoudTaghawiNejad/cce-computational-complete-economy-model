from __future__ import division
from firm import Firm
from household import Household
from netexport import NetExport
from government import Government
from abce import Simulation
from collections import OrderedDict, defaultdict
import os
from sam_to_functions import Sam
from pprint import pprint
import iotable
from scipy import optimize



def main(money):
    sam = Sam('climate_square.sam.csv',
              inputs=['col', 'ele', 'gas', 'o_g', 'oil', 'eis', 'trn', 'roe', 'lab', 'cap'],
              outputs=['col', 'ele', 'gas', 'o_g', 'oil', 'eis', 'trn', 'roe'],
              output_tax='tax',
              consumption=['col', 'ele', 'gas', 'o_g', 'oil', 'eis', 'trn', 'roe'],
              consumers=['hoh'])
    """ reads the social accounting matrix and returns coefficients of a cobb-douglas model """
    carbon_prod = defaultdict(float)
    carbon_prod.update({'col': 2112 * 1e-4,
                        'oil': 2439.4 * 1e-4,
                        'gas': 1244.3 * 1e-4})
    """ this is the co2 output per sector at the base year """
    print 'carbon_prod'
    print carbon_prod


    simulation_parameters = {'name': 'cce',
                             'random_seed': None,
                             'num_rounds': 500,
                             'tax_change_time': 400,
                             'num_household': 1,
                             'num_firms': 1,
                             'endowment_FFcap': sam.endowment('cap'),
                             'endowment_FFlab': sam.endowment('lab'),
                             'final_goods': sam.consumption,
                             'capital_types': ['cap', 'lab'],
                             'wage_stickiness': 0.50,
                             'price_stickiness': 0.50,
                             'network_weight_stickiness': 0.0,
                             'import_price_stickiness': 0.0,
                             'dividends_percent': 0.0,
                             'production_functions': sam.production_functions(),
                             'consumption_functions': sam.utility_function(),
                             'output_tax_shares': sam.output_tax_shares(),
                             'money': money,
                             'inputs': sam.inputs,
                             'outputs': sam.outputs,
                             'balance_of_payment': sam.balance_of_payment('nx', 'inv'),
                             'sam': sam,
                             'carbon_prod': carbon_prod,
                             'carbon_tax': 50}



    simulation = Simulation(rounds=simulation_parameters['num_rounds'], trade_logging='group', processes=1)

    simulation.declare_service('endowment_FFcap', 1, 'cap')
    simulation.declare_service('endowment_FFlab', 1, 'lab')
    """ every round for every endowment_FFcap the owner gets one good of lab
    similar for cap"""

    simulation.aggregate('household',
                         possessions=['money', 'labor'],
                         variables=['sales_earning', 'rationing', 'welfare'])
    """ collect data """

    simulation.aggregate('government',
                         variables=['money'])

    for good in sam.outputs:
        simulation.aggregate(good,
                             possessions=['money'],
                             variables=['price', 'produced', 'co2'])
    #for good in carbon_prod:
    #    simulation.aggregate(good,
    #                         variables=['co2'])

    firms = sum([simulation.build_agents(Firm,
                                     number=simulation_parameters['num_firms'],
                                     group_name=good,
                                     parameters=simulation_parameters)
             for good in sam.outputs])
    household = simulation.build_agents(Household, 'household', simulation_parameters['num_household'], parameters=simulation_parameters)
    netexport = simulation.build_agents(NetExport, 'netexport', 1, parameters=simulation_parameters)
    government = simulation.build_agents(Government, 'government', 1, parameters=simulation_parameters)

    firms_and_household = firms + household

    """ this is the execution order every round """
    try:
        for r in simulation.next_round():
                       firms.do('taxes_intervention')
                       firms_and_household.do('send_demand')
                       firms_and_household.do('selling')
                       firms_and_household.do('buying')
                       household.do('money_to_nx')
                       firms.do('production')
                       firms.do('carbon_taxes')
                       firms.do('sales_tax')
                       government.do('taxes_to_household')
                       firms.do('international_trade')
                       firms.do('invest')
                       netexport.do('invest')
                       household.do('sales_accounting')
                       firms.do('dividends')
                       firms.do('change_weights')
                       firms.do('stats')
                       firms_and_household.do('aggregate')
                       household.do('consuming')
                       government.do('aggregate')
    except Exception as e:
        print(e)
    simulation.graphs()
        # raise  # put raise for full traceback but no graphs in case of error
    iotable.to_iotable(simulation.path, [simulation_parameters['tax_change_time'] - 1, simulation_parameters['num_rounds'] - 1])
    mean_price = iotable.average_price(simulation.path, simulation_parameters['tax_change_time'] - 1)
    print 'mean price', mean_price
    print iotable.co2(simulation.path, simulation_parameters['tax_change_time'] - 1, simulation_parameters['num_rounds'] - 2)
    return mean_price

def F(money):
    prices = main(float(money))
    print("****")
    print 'money', money
    print 'price lvl', prices
    print("****")
    return ((1.0 - prices) ** 2) * 100000

if __name__ == '__main__':
    main(2691.2641884030372)
    #opt =  optimize.minimize_scalar(F, bracket=(2685, 2750), bounds=(2685, 2750), method='brent', options={'xtol': 0.000000000001})
    #print opt
